package model;

import interfaces.Measurable;

public class Data {
	public static double average(Measurable[] obj){
		double sum = 0;
		for (Measurable m: obj){
			sum += m.getMeasure();
		}
		if(obj.length>0){
			sum = sum/obj.length;
		}
		return sum;
	}

	public static Measurable min(Measurable m1, Measurable m2){
		//double minn = 0;
		if (m1.getMeasure() < m2.getMeasure()){
			//minn = m1.getMeasure();
			return m1;
		}
		else{
			//minn = m2.getMeasure();
			return m2;
		}
		//return 0;
	}
}
