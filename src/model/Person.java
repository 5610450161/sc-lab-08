package model;

import interfaces.Measurable;
import interfaces.Taxable;

public class Person implements Measurable , Taxable {
	String name;
	double tall;
	double annualSalary;
	
	public Person(String name, double tall, double annualSalary){
		this.name = name;
		this.tall = tall;
		this.annualSalary = annualSalary;
	}
	
	public double getMeasure(){
		return this.tall;
	}
	
	//public String getName(){
	//	return this.name;
	//}
	public String toString(){
		return name +" "+tall;
	}

	@Override
	public double getTax() {
		double tax = 0;
		double annual = 0;
		if(Math.floor(this.annualSalary) <= 300000){
			tax = this.annualSalary * 0.05;
		}
		else{
			tax = 300000 * 0.05;
			annual = this.annualSalary - 300000;
			tax = tax + (annual * 0.1);
		}
		return tax;
	}
}
