package control;

import interfaces.Measurable;
import model.BankAccount;
import model.Company;
import model.Country;
import model.Data;
import model.Person;
import model.Product;
import model.TaxCalculator;
import interfaces.Taxable;
public class Test {
	public static void main(String[] arg){
		Test test = new Test();
		System.out.println("Test Person");
		test.testPerson();
		System.out.println("----------------------------");
		System.out.println("Test Min");
		test.testMin();
		System.out.println("----------------------------");
		System.out.println("Test Tax");
		test.testTax();
	}
	
	public void testPerson(){
		Measurable[] persons = new Measurable[3];
		persons[0] = new Person("a",150,100000);
		persons[1] = new Person("b",160,200000);
		persons[2] = new Person("c",170,300000);

		System.out.println("Person average tall : "+Data.average(persons));
	}
	
	public void testMin(){
		Measurable[] persons = new Measurable[6];
		Measurable[] persons2 = new Measurable[3];
		persons[0] = new Person("a",150,100000);
		persons[1] = new Person("b",180,200000);
		persons[2] = new Country("Thai", 50000);
		persons[3] = new Country("USA", 55000);
		persons[4] = new BankAccount("c",1000);
		persons[5] = new BankAccount("d",2000);
		
		persons2[0] = Data.min(persons[0],persons[1]);
		persons2[1] = Data.min(persons[2],persons[3]);
		persons2[2] = Data.min(persons[4],persons[5]);
		System.out.println("Average tall : "+Data.average(persons2));
	}
	
	public void testTax(){
		Person[] persons = new Person[2];
		Company[] companys = new Company[2];
		Product[] products = new Product[2];
		Taxable[] taxable = new Taxable[3];
		
		persons[0] = new Person("a",150,300000);
		persons[1] = new Person("b",180,500000);
		
		companys[0] = new Company("Company1",1000000,800000);
		companys[1] = new Company("Company2",1000000,200000);
		
		products[0] = new Product("item1",100);
		products[1] = new Product("item2",700);
		
		taxable[0] = new Person("a",150,300000);
		taxable[1] = new Company("Company1",1000000,800000);
		taxable[2] = new Product("item1",100);
		
		System.out.println("Person sum tax : "+TaxCalculator.sum(persons));
		System.out.println("Company sum tax : "+TaxCalculator.sum(companys));
		System.out.println("Product sum tax : "+TaxCalculator.sum(products));
		System.out.println("All class sum tax : "+TaxCalculator.sum(taxable));
	}
}
